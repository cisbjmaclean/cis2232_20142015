/**
 * This class will contain code assiated with the Pin number required to check in
 * to the class attendance.
 */
package util;

import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author bjmaclean
 */
public class Pin {

    String name;
    String courseName;
    int pinEntered;
    boolean validated = false;

    public Pin(String name, String courseName, int pinEntered) {
        this.name = name;
        this.courseName = courseName;
        this.pinEntered = pinEntered;
    }

    public boolean isValidated() {
        return validated;
    }

    public void setValidated(boolean validated) {
        this.validated = validated;
    }

    
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public int getPinEntered() {
        return pinEntered;
    }

    public void setPinEntered(int pinEntered) {
        this.pinEntered = pinEntered;
    }
    
    
    
    /**
     *
     * This method will provide the PIN for a given course. This will be a hash
     * value based on the date and the course code.
     *
     * @param courseCode
     * @return
     */
    public static int getTodaysPin(String courseCode) {

        int pin = 0;
        int totalOfAsci = 0;

        // get asci of cousre code
        for (int x = 0; x < courseCode.length(); x++) {
            totalOfAsci += (int) courseCode.charAt(x);
        }

        //  Get code from date
        Date date = null; // your date
        Calendar cal = Calendar.getInstance();
        // cal.setTime(date);
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        
        // Add totals
        
        pin = totalOfAsci * (year + month + day );
        
        System.out.println(pin);

        // The daily pin will be generated based on the sum of the ascii codes of the 
        // course code multiplied by the sum of the digits in today's date (yyyymmdd format).
        // To limit the length of the pin, the result should never be more than six digits.  
        //generate the code for this based on the description above.
        return pin;

    }

    public static boolean validatePin(String courseName, String pin){
        
        return pin.equals(String.valueOf(getTodaysPin(courseName)));
    }
}
